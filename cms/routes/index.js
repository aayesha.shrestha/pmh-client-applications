var express = require('express');
var router = express.Router();

const alert = require('alert-node');
const axios = require('axios');
const nodemailer = require('nodemailer');

const smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
        user: "infos.pastel@gmail.com",
        pass: "pastel@123"
    }
});

var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/license')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + Date.now())
    }
});
var upload = multer({storage: storage});

router.get('/', function(req, res, next) {
  if(req.session.adminName && req.session.token){
    res.redirect('/doctorReg');
  }else{
    res.redirect('/login');
  }
});

router.get('/login', function(req, res, next) {
  res.render('login');
});

router.get('/doctorReg', function(req, res, next) {
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }
    res.render('doctorReg');
});

router.get('/patientReg', function(req, res, next) {
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }
    res.render('patientReg');
});

router.post('/login', async function(req, res, next) {
    if(req.body.username == "admin" && req.body.password == "adminpw"){
        try {
            const formData = {
                username : req.body.username,
                password : req.body.password
            }
            let payload = {};
            payload.method = "POST";
            payload.data = formData;
            payload.mode = 'no-cors';
            payload.withCredentials = true;
            payload.credentials = 'same-origin';
            payload.url = "http://167.71.98.0:4000/login";
            let response = await axios(payload);
            let data = response.data;

            if(data.success == true){
                req.session.adminName = req.body.username;
                req.session.token = data.token;
                req.session.save();
                res.redirect('/doctorReg');
            }else{
                alert("internal server error!");
            }
        } catch (e) {
            next();
        }
    }else {
        alert('You cannot login as admin. ');
        res.redirect('/login')
    }
});

router.post('/doctorRegistration', async function(req, res, next) {
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }

    try{
        const formData = {
                    username : "Dr-" + req.body.firstname + "-" + req.body.lastname,
                    permissions : []
                };
        let payload = {};
        payload.method = "POST";
        payload.data = formData;
        payload.mode = 'no-cors';
        payload.withCredentials = true;
        payload.credentials = 'same-origin';
        payload.url = "http://167.71.98.0:4000/register";
        var bearer = 'Bearer ' + req.session.token;
        payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'};
        let response = await axios(payload);
        let data = response.data;

        if(data.success == true){
            let msg = data.message;
            let checkMsg = msg.split(':');
            if(checkMsg[0] == 'Failed to register'){
                alert("Username already exist");
                res.redirect('/doctorReg');
            }else{
                var mailText = "Hello, " + req.body.firstname + ". Your DOCTOR account has been successfully created with following details. \n Username: "
                    + "Dr-" + req.body.firstname + "-" + req.body.lastname + "\n" + "Password : " + data.message;
                var mailOptions={
                    to : req.body.email,
                    subject : 'Account Details',
                    text : mailText
                };

                smtpTransport.sendMail(mailOptions, function(error, response){
                    if(error){
                        console.log("error.....", error);
                    }else{
                        console.log("Message sent: " + response);
                        alert("Account detail has been mailed to " + req.body.email);
                    }
                });

                try{
                    let licenseData = {"args": {
                            licenseUUID: req.body.licenseNo,
                            name: "Dr-" + req.body.firstname + "-" + req.body.lastname,
                            licenseNo: req.body.licenseNo,
                            address: req.body.address,
                            scannedLicense: "license" }}
                    let payloadd = {};
                    payloadd.method = "POST";
                    payloadd.data = licenseData;
                    payloadd.mode = 'no-cors';
                    payloadd.withCredentials = true;
                    payloadd.credentials = 'same-origin';
                    payloadd.url = "http://167.71.98.0:4000/api/license_register";
                    const bearer = 'Bearer ' + req.session.token;
                    payloadd.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
                    let response = await axios(payloadd);
                    let data = response.data;
                    alert("doctor account registered");
                    res.redirect("/doctorReg");
                }catch (e) {
                    console.log(e)
                }
            }
        }else{
            alert("Server error!");
        }
    }catch (e) {
        console.log(e);
    }
});

router.post('/patientRegistration', async function(req, res, next) {
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }

    try{
        const formData = {
            username : "Patient-" + req.body.firstname + "-" + req.body.lastname,
            permissions : []
        }
        let payload = {};
        payload.method = "POST";
        payload.data = formData;
        payload.mode = 'no-cors';
        payload.withCredentials = true;
        payload.credentials = 'same-origin';
        payload.url = "http://167.71.98.0:4000/register";
        var bearer = 'Bearer ' + req.session.token;
        payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
        let response = await axios(payload);
        let data = response.data;

        if(data.success == true){
            var mailText = "Hello, " + req.body.firstname + ". Your PATIENT account has been successfully created with following details. \n Username: "
                + "Patient-" + req.body.firstname + "-" + req.body.lastname + "\n" + "Password : " + data.message;
            var mailOptions={
                to : req.body.email,
                subject : 'Account Details',
                text : mailText
            }

            smtpTransport.sendMail(mailOptions, function(error, response){
                if(error){
                    console.log("error.....", error);
                    alert("error");
                }else{
                    console.log("Message sent: " + response);
                    alert("Account detail has been mailed to " + req.body.email);
                }
            });
            try {
                let recordData = {"args": {
                    "recordUUID": req.body.phone, "holderName": "Patient-" + req.body.firstname + "-" + req.body.lastname, "gender": req.body.gender, "address": req.body.address, "holderDOB": req.body.dob, "height": "5ft 4 inches", "bloodGroup": req.body.bloodGroup, "issuer": req.body.issuer, "issuedDate": "2019-Sept-29", "emergencyContact":{ "ContactName": req.body.emergencyName, "ContactNumber": req.body.emergencyContact } }}
                let payloadd = {};
                payloadd.method = "POST";
                payloadd.data = recordData;
                payloadd.mode = 'no-cors';
                payloadd.withCredentials = true;
                payloadd.credentials = 'same-origin';
                payloadd.url = "http://167.71.98.0:4000/api/record_register";
                const bearer = 'Bearer ' + req.session.token;
                payloadd.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
                let response = await axios(payloadd);
                let data = response.data;
                console.log(data);
                res.redirect("/patientReg");
            } catch (e) {
                console.log(e)
            }
        }
    }catch (e) {
        console.log(e)
    }
    res.redirect('/patientReg');
});

router.get('/main', function(req, res, next) {
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }
    res.render('doctorReg');
});

router.get('/patientsList', async function(req, res, next) {
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }

    try {
        let formData = {
            "args":{

            }
        }
        let payload = {};
        payload.method = "POST";
        payload.data = formData;
        payload.mode = 'no-cors';
        payload.withCredentials = true;
        payload.credentials = 'same-origin';
        let bearer = 'Bearer ' + req.session.token;
        payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
        payload.url = "http://167.71.98.0:4000/api/query_records";
        let response = await axios(payload);
        let data = response.data;

        if(data.success == true){
            res.render('patients', { patientsList : JSON.parse(data.message)});
        }else{
            alert("Server error");
            res.redirect('/doctorReg');
        }
    } catch (e) {
        console.log(e);
    }
});

router.get('/doctorsList', async function(req, res, next) {
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }

    try {
        let formData = {
            "args":{

            }
        }
        let payload = {};
        payload.method = "POST";
        payload.data = formData;
        payload.mode = 'no-cors';
        payload.withCredentials = true;
        payload.credentials = 'same-origin';
        let bearer = 'Bearer ' + req.session.token;
        payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
        payload.url = "http://167.71.98.0:4000/api/query_licenses";
        let response = await axios(payload);
        let data = response.data;

        if(data.success == true){
            res.render('doctors', { doctorsList : JSON.parse(data.message)});
        }else{
            alert("Server error");
            res.redirect('/doctorReg');
        }
    } catch (e) {
        console.log(e);
    }

});

router.get('/singlePatient/:id', async function(req, res, next) {
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }

    let pmhId = req.params.id;
    let record = {};
    let reports = [];
    let prescriptions = [];
    let all_descriptions = [];
    let report_count = 0;
    let pres_count = 0;
    try {
        let formData = {
            "args":{

            }
        }
        let payload = {};
        payload.method = "POST";
        payload.data = formData;
        payload.mode = 'no-cors';
        payload.withCredentials = true;
        payload.credentials = 'same-origin';
        let bearer = 'Bearer ' + req.session.token;
        payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
        payload.url = "http://167.71.98.0:4000/api/query_records";
        let response = await axios(payload);
        let data = response.data;

        if(data.success == true){
            const all_records = JSON.parse(data.message);
            for(var i=0; i<all_records.length; i+=1){
                if(all_records[i].holderName == pmhId){
                    record = all_records[i];
                }
            }
            if(record.holderName == undefined){
                alert("Patient not found");
                res.redirect('/patientsList');
            }
        }else{
            alert("Cannot find your data.");
        }
    } catch (e) {
        console.log(e);
    }

    try {
        let formData = {
            "args":{

            }
        };
        let payload = {};
        payload.method = "POST";
        payload.data = formData;
        payload.mode = 'no-cors';
        payload.withCredentials = true;
        payload.credentials = 'same-origin';
        let bearer = 'Bearer ' + req.session.token;
        payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
        payload.url = "http://167.71.98.0:4000/api/query_reports";
        let response = await axios(payload);
        let data = response.data;

        if(data.success == true){
            let all_reports = JSON.parse(data.message);

            for(let i=0; i<all_reports.length; i+=1){
                if(all_reports[i].pmhID == pmhId){
                    reports.push(all_reports[i]);
                }
            }

            report_count = reports.length
        }else{
            alert("Please try again after sometime.")
        }
    } catch (e) {
        console.log(e);
    }

    try {
        let formData = {
            "args":{

            }
        }
        let payload = {};
        payload.method = "POST";
        payload.data = formData;
        let bearer = 'Bearer ' + req.session.token;
        payload.mode = 'no-cors';
        payload.withCredentials = true;
        payload.credentials = 'same-origin';
        payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
        payload.url = "http://167.71.98.0:4000/api/query_prescriptions";
        let response = await axios(payload);
        let data = response.data;

        if(data.success == true){
            let all_prescriptions = JSON.parse(data.message);

            for(let i=0; i<all_prescriptions.length; i+=1){
                if(all_prescriptions[i].pmhID == pmhId){
                    prescriptions.push(all_prescriptions[i]);
                    all_descriptions.push(all_prescriptions[i].description);
                }
            }

            pres_count = prescriptions.length
        }else{
            alert("Please try again after sometime.")
        }
    } catch (e) {
        console.log(e);
    }

    res.render('singlePatient', { record: record, reports: reports, prescriptions: prescriptions, name: req.session.doctorName, prescriptionCount: pres_count, reportCount: report_count });

});

router.get('/singleDoctor/:id', async function(req, res, next){
    if(!req.session.adminName || !req.session.token){
        res.redirect('/login');
    }

    let doctorName = req.params.id;
    try {
        let formData = {
            "args":{

            }
        };

        let payload = {};
        payload.method = "POST";
        payload.data = formData;
        payload.mode = 'no-cors';
        payload.withCredentials = true;
        payload.credentials = 'same-origin';
        let bearer = 'Bearer ' + req.session.token;
        payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
        payload.url = "http://167.71.98.0:4000/api/query_licenses";
        let response = await axios(payload);
        let data = response.data;

        let doctorInfo;
        if(data.success == true){
            const all_license = JSON.parse(data.message);
            for(var i=0; i<all_license.length; i+=1){
                if(all_license[i].name == doctorName){
                    doctorInfo = all_license[i];
                }
            }
            if(doctorInfo == undefined){
                alert("Cannot find data");
                res.redirect('/doctorsList');
            }else{
                res.render('singleDoctor', { license: doctorInfo });
            }
        }else{
            alert("Try again later.");
            res.redirect('/doctorsList');
        }
    } catch (e) {
        console.log(e);
    }
});

router.post('/searchPatient', function(req, res, next){
   res.redirect('/singlePatient/'+req.body.patientName);
});

router.post('/searchDoctor', function(req, res, next){
    res.redirect('/singleDoctor/'+req.body.doctorName);
});

module.exports = router;
