var express = require('express');
var router = express.Router();

const axios = require('axios');
const alert = require('alert-node');


/* GET home page. */
router.get('/', function(req, res, next) {
  if(req.session.doctorName && req.session.token){
    res.redirect('/main');
  }else{
    res.redirect('/login');
  }
});

router.get('/login', function(req, res, next){
  res.render('login');
});

router.get('/main', async function(req, res, next){
  // if(!req.session.doctorName || !req.session.token){
  //     req.session.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU3MTE0NDY1NDUsInVzZXJuYW1lIjoiRHItZmFrc2FzLWpuY2FzIiwiaWF0IjoxNTcxNDQ2NTQ1fQ.pv_iVIkugvkUH8X_fGzfvEz_Cr85fnJA7GV73TEu1T0";
  //     req.session.doctorName = "Dr-faksas-jncas"
  // }

  if(!req.session.doctorName || !req.session.token){
    res.redirect('/login');
  }

  try {
    let formData = {
      "args":{

      }
    };
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/query_licenses";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      const all_license = JSON.parse(data.message);
      for(var i=0; i<all_license.length; i+=1){
        if(all_license[i].name == req.session.doctorName){
          res.render('myProfile', { license: all_license[i] });
        }
      }
    }else{
      alert("Cannot find your data.");
    }
  } catch (e) {
    console.log(e);
  }
});

router.post('/login', async function(req, res, next){
  var username = req.body.username;
  var role = username.substring(0,2);
  if (role != "Dr"){
    alert("Please make sure it is your doctor account.")
    res.render('login');
  }

  try {
    const formData = {
      username : req.body.username,
      password : req.body.password
    }
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    payload.url = "http://167.71.98.0:4000/login";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      req.session.doctorName = req.body.username;
      req.session.token = data.token;
      req.session.save();
      res.redirect('/main');
    }else{
      alert("username or password incorrect");
    }
  } catch (e) {
    console.log(e);
  }
});

router.get('/report', function(req, res, next){
  if(!req.session.doctorName || !req.session.token){
    res.redirect('/login');
  }
  res.render('reportForm', { name: req.session.doctorName });
});

router.get('/prescription', function(req, res, next){
  if(!req.session.doctorName || !req.session.token){
    res.redirect('/login');
  }
  res.render('prescriptionForm', { name: req.session.doctorName });
});

router.post('/addReport', async function(req, res, next){
  if(!req.session.doctorName || !req.session.token){
    res.redirect('/login');
  }

  let reportNumber = 0;
  try {
    let formData = {
      "args":{

      }
    };
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/query_reports";
    let response = await axios(payload);
    let data = response.data;

    let all_reports = JSON.parse(data.message);
    reportNumber = all_reports.length;
  } catch (e) {
    console.log(e);
  }

  try {
    let formData = {
      "args": {
        "reportUUID": reportNumber.toString(),
        "pmhID": req.body.patientName,
        "reportName": req.body.reportName,
        "reportType": "SCANNED",
        "file": "/tmp/report/001",
        "issuedDate": req.body.issuedDate,
        "referredDoctor": req.body.issuer,
        "description": req.body.editor1
      }
    };
    let payload = {};
    payload.method = "POST";
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    payload.data = formData;
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/add_report";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      alert("report added.");
      res.redirect('/main');
    }else{
      alert("something went wrong.")
    }
  } catch (e) {
      console.log(e)
  }
});

router.post('/addPrescription', async function(req, res, next){
  if(!req.session.doctorName || !req.session.token){
    res.redirect('/login');
  }

  let prescriptionNumber = 0;
  try {
    let formData = {
      "args":{

      }
    };
    let payload = {};
    payload.method = "POST";
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    payload.data = formData;
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'};
    payload.url = "http://167.71.98.0:4000/api/query_prescriptions";
    let response = await axios(payload);
    let data = response.data;

    let all_prescriptions = JSON.parse(data.message);
    prescriptionNumber = all_prescriptions.length;
  } catch (e) {
    console.log(e);
  }

  let drugs_in_comma = req.body.drugsName;
  let drugs = drugs_in_comma.split(',');

  try {
    let formData = {
      "args":{
        "prescriptionUUID": prescriptionNumber.toString(),
        "pmhID": req.body.patientName,
        "drugs": drugs,
        "prescriptionType":"SCANNED",
        "file":"/tmp/report/001",
        "issuedDate": req.body.issuedDate,
        "issuedBy": req.body.issuer,
        "description": req.body.editor1,
        "voidTill": req.body.date_of_validity,
        "received": false
      }
    };
    let payload = {};
    payload.method = "POST";
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    payload.data = formData;
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/add_prescription";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      alert("report added.");
      res.redirect('/main');
    }else{
      alert("something went wrong.")
    }
  } catch (e) {
    console.log(e)
  }
});

router.get('/patientInfoForm', function(req, res){
  if(!req.session.doctorName || !req.session.token){
    res.redirect('/login');
  }
  res.render('getPatientInfo', { name: req.session.doctorName });
});

router.post('/getPatient', async function(req, res){
  if(!req.session.doctorName || !req.session.token){
    res.redirect('/login');
  }

  let pmhId = req.body.patientName;
  let record = {};
  let reports = [];
  let prescriptions = [];
  let all_descriptions = [];
  let report_count = 0;
  let pres_count = 0;
  try {
    let formData = {
      "args":{

      }
    }
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/query_records";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      const all_records = JSON.parse(data.message);
      for(var i=0; i<all_records.length; i+=1){
        if(all_records[i].holderName == pmhId){
          record = all_records[i];
        }
      }
      if(record.holderName == undefined){
        alert("Patient not found");
        res.redirect('/patientInfoForm');
      }
    }else{
      alert("Cannot find your data.");
    }
  } catch (e) {
    console.log(e);
  }

  try {
    let formData = {
      "args":{

      }
    };
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/query_reports";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      let all_reports = JSON.parse(data.message);

      for(let i=0; i<all_reports.length; i+=1){
        if(all_reports[i].pmhID == pmhId){
          reports.push(all_reports[i]);
        }
      }

      report_count = reports.length
    }else{
      alert("Please try again after sometime.")
    }
  } catch (e) {
    console.log(e);
  }

  try {
    let formData = {
      "args":{

      }
    }
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    let bearer = 'Bearer ' + req.session.token;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/query_prescriptions";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      let all_prescriptions = JSON.parse(data.message);

      for(let i=0; i<all_prescriptions.length; i+=1){
        if(all_prescriptions[i].pmhID == pmhId){
          prescriptions.push(all_prescriptions[i]);
          all_descriptions.push(all_prescriptions[i].description);
        }
      }

      pres_count = prescriptions.length
    }else{
      alert("Please try again after sometime.")
    }
  } catch (e) {
    console.log(e);
  }

  res.render('patientInfo', { record: record, reports: reports, prescriptions: prescriptions, name: req.session.doctorName, prescriptionCount: pres_count, reportCount: report_count });

});

router.post('/requestMedicine', async function(req, res){
  if(!req.session.doctorName || !req.session.token){
    res.redirect('/login');
  }

  var presKey = req.body.presKey;
  var key_arr = presKey.split('�');
  var prescriptionKey = '\u0000' + key_arr[1] + '\u0000' + key_arr[2] + '\u0000'
  console.log(key_arr);
  try {
    let formData = {
      "args":{
        "prescriptionUUID": prescriptionKey
      }
    };
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/request_medicine";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      alert("Updated!! Enter patient id to check.");
      res.redirect('/patientInfoForm');
    }else{
      alert("Cannot find your data.");
    }
  } catch (e) {
    console.log(e);
  }
});

module.exports = router;
