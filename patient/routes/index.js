var express = require('express');
var router = express.Router();

const axios = require('axios');
const alert = require('alert-node');

axios.defaults.withCredentials = true;

/* GET home page. */
router.get('/', function(req, res, next) {
  if(req.session.patientName && req.session.token){
    res.redirect('/profile');
  }else{
    res.redirect('/login');
  }
});

router.get('/login', function(req, res, next){
  res.render('login');
});

router.get('/profile', async function(req, res, next){
  // if(!req.session.patientName || !req.session.token){
  //   req.session.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjU3MTE0OTY4MzAsInVzZXJuYW1lIjoiUGF0aWVudC1qaGJrYXNtbC11aW9wbDsiLCJpYXQiOjE1NzE0OTY4MzB9.BG4NE3KqTcICZtWvT7kKDGd1dY2Rm6URDx_1GodVqRY";
  //   req.session.patientName = "Patient-jhbkasml-uiopl;"
  // }

  if(!req.session.patientName || !req.session.token){
    res.redirect('/login');
  }

  try {
    let formData = {
      "args":{

      }
    }
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/query_records";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      const all_records = JSON.parse(data.message);
      for(var i=0; i<all_records.length; i+=1){
        if(all_records[i].holderName == req.session.patientName){
          console.log(all_records[i]);
          res.render('myProfile', { record: all_records[i] });
        }
      }
    }else{
      alert("Cannot find your data.");
    }
  } catch (e) {
    console.log(e);
  }
});

router.post('/login', async function(req, res, next){
  var username = req.body.username;
  var role = username.substring(0,7);
  console.log(role)
  if (role != "Patient"){
    alert("Please make sure it is your patient account.")
    res.render('login');
  }else{
    try {
      const formData = {
        username : req.body.username,
        password : req.body.password
      }
      let payload = {};
      payload.method = "POST";
      payload.data = formData;
      payload.mode = 'no-cors';
      payload.withCredentials = true;
      payload.credentials = 'same-origin';
      payload.url = "http://167.71.98.0:4000/login";
      let response = await axios(payload);
      let data = response.data;

      if(data.success == true){
        req.session.patientName = req.body.username;
        req.session.token = data.token;
        req.session.save();
        console.log(data)
        res.redirect('/profile');
      }else{
        alert("username or password incorrect");
      }
    } catch (e) {
      console.log(e);
    }
  }


});

router.get('/prescriptions', async function(req, res){
  if(!req.session.patientName || !req.session.token){
    res.redirect('/login');
  }

  try {
    let formData = {
      "args":{

      }
    }
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/query_prescriptions";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      let all_prescriptions = JSON.parse(data.message);
      let my_prescriptions = [];

      for(let i=0; i<all_prescriptions.length; i+=1){
        if(all_prescriptions[i].pmhID == req.session.patientName){
          my_prescriptions.push(all_prescriptions[i]);
        }
      }

      let pres = my_prescriptions.length
      res.render('prescriptions', { prescriptions: my_prescriptions, username: req.session.patientName, presCount: pres });
    }else{
      alert("Please try again after sometime.")
    }
  } catch (e) {
    console.log(e);
  }
});

router.get('/reports', async function(req, res){
  if(!req.session.patientName || !req.session.token){
    res.redirect('/login');
  }

  try {
    let formData = {
      "args":{

      }
    };
    let payload = {};
    payload.method = "POST";
    payload.data = formData;
    payload.mode = 'no-cors';
    payload.withCredentials = true;
    payload.credentials = 'same-origin';
    let bearer = 'Bearer ' + req.session.token;
    payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
    payload.url = "http://167.71.98.0:4000/api/query_reports";
    let response = await axios(payload);
    let data = response.data;

    if(data.success == true){
      let all_reports = JSON.parse(data.message);
      let my_reports = [];

      for(let i=0; i<all_reports.length; i+=1){
        if(all_reports[i].pmhID == req.session.patientName){
          my_reports.push(all_reports[i]);
        }
      }

      let repCount = my_reports.length
      res.render('reports', { reports: my_reports, username: req.session.patientName, reportCount: repCount });
    }else{
      alert("Please try again after sometime.")
    }
  } catch (e) {
    console.log(e);
  }
});

// router.post('/grantAccess', async function(req, res, next){
//   try {
//     var recordKey = req.body.recordKey;
//     var recordKeySplit = recordKey.split("�");
//     const formData = {
//       "args":{
//         "accessUUID": recordKeySplit[2],
//         "pmhId": req.session.patientName,
//         "licenseNo": req.body.licenseNo
//       }
//     }
//     let payload = {};
//     payload.method = "POST";
//     payload.data = formData;
//     let bearer = 'Bearer ' + req.session.token;
//     payload.headers = {'Authorization': bearer, 'Content-Type': 'application/json'}
//     payload.url = "http://167.71.98.0:4000/api/grant_access";
//     let response = await axios(payload);
//     let data = response.data;
//
//     if(data.success == true){
//       alert("Access Granted");
//       res.redirect("/profile");
//     }else{
//       alert("Are you sure the license number is  correct?");
//     }
//   } catch (e) {
//     console.log(e);
//   }
// });

module.exports = router;
